my_age = 21 
age = int(input('What is your age: \n'))

if age < my_age:
    print(f"you are {my_age - age} year younger than me")
elif age > my_age:
    print(f"you are {age - my_age} year younger than me")
else:
    print("We have the same age :D")
