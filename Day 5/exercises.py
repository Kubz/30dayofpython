lista_vacia = []
cinco_item = ['item1', 'item2', 'item3', 'item4', 'item5', 'item6']
print(f"La longitud de cinco_item es {len(cinco_item)}")
print(f'''el primer item es: {cinco_item[0]}, el de en medio es: {cinco_item[3]}
y el ultimo es: {cinco_item[-1]} ''')
#5 
mixed_data_types = ["Enrique", 1,81, "Managua", 15]
#6
it_companies = ["Facebook", "Google", "Microsoft", "Apple", "IBM", "Oracle", "Amazon"]
print(f"Algunas de las compañias de tecnología son: {it_companies}")
print(it_companies[0], it_companies[3], it_companies[-1])
#11
it_companies.append("Github")
print(len(it_companies))
it_companies.insert(3, "Netflix")
print("Facebook" in it_companies)
#16
it_companies.sort()
print(f"La lista ordenada: {it_companies}")
it_companies.reverse()
print(f"la lista ordenada al revés es: {it_companies}")
#18
print(it_companies)
print(f"Los primeros 3 items de la lista son: {it_companies[0:2]}")
print(f"Los ultimos 3 items de la lista son: {it_companies[-3:]}")
it_companies.pop(3)
it_companies.pop(0)
it_companies.pop()
print(f"Lista luego de borrar algunos elementos: {it_companies}")
it_companies.clear()
del it_companies

front_end = ['HTML', 'CSS', 'JS', 'React', 'Redux']
back_end = ['Node', 'Express', "MongoDB"]

fullstack = front_end + back_end
print(fullstack)
fullstack.copy()
fullstack.insert(5, "Python")
fullstack.insert(6, "SQL")
print(fullstack)
