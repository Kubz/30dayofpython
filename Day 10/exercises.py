for a in range(10):
    print(a)




for a in range(10, 0, -1):
    print(a)



for a in range(8):
    print("#" * a)


for a in range(8):
    print(8 * '# ')


for a in range (10):
    print(f"{9} x {a+1} = {9* (a+1)}")


python = ['Python','Numpy','Pandas','Django','Flask']

for tec in python:
    print(tec)

for a in range(100):
    if a % 2 == 0:
        print(a, end=",")

print('\n')

for a in range(100):
    if a % 2 != 0:
        print(a, end=",")

sum = 0
for a in range(101):
    sum += a

print(f"\n the sum of the first 100 numbers is: {sum}")
